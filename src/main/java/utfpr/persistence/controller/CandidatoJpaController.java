package utfpr.persistence.controller;

import inscricao.persistence.entity.Idioma;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * Desenvolvimento de aplicações Web
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class CandidatoJpaController extends JpaController {

    public CandidatoJpaController() {
    }
    
    public List<Idioma> findbyIdioma(Idioma idioma) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Idioma> q = em.createQuery(
                "select c from Candidato c where c.idioma = :idioma order by c.nome",
                Idioma.class);
            return q.getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public List<Idioma> findbyIdioma(Integer codigo) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Idioma> q = em.createQuery(
                "select c from Candidato c where c.codigo = :codigo order by c.nome",
                Idioma.class);
            return q.getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public List<Idioma> findbyIdioma() {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Idioma> q = em.createQuery(
                "select c from Candidato c order by c.nome",
                Idioma.class);
            return q.getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
    
}
